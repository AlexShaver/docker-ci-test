FROM alpine

RUN echo "Hello World\n2" >> /hello_world

RUN apk update && apk add nano

RUN echo "Hello World6" >> /hello_world2

RUN apk add openssh

RUN echo "Hello World" >> /hello_world3 \
    echo "Hello World 2" >> /hello_world4
